<?php

/*
 * @category  Projects
 * @package   self.socialFella
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2014 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 * Description of CodebirdWrapper
 *
 * @author vladislav
 */

require_once Yii::getPathOfAlias('codebird').'/codebird.php';

use Codebird\Codebird;

class CodebirdWrapper extends Codebird {
    
    const CATEGORY_APPLICATION  = 'application';
    const CATEGORY_FAVORITES    = 'favorites';
    const CATEGORY_FOLLOWERS    = 'followers';
    const CATEGORY_FRIENDS      = 'friends';
    const CATEGORY_FRIENDSHIPS  = 'friendships';
    const CATEGORY_HELP         = 'help';
    const CATEGORY_LISTS        = 'lists';
    const CATEGORY_MEDIA        = 'media';
    const CATEGORY_SEARCH       = 'search';
    const CATEGORY_STATUSES     = 'statuses';
    const CATEGORY_TRENDS       = 'trends';
    const CATEGORY_USERS        = 'users';
    const CATEGORY_AUTH         = 'auth';
    
    const ERROR_BLOCKED = 136;
    
    /**
     * The current singleton instance
     */
    private static $_instance = null;
    
    /**
     * Returns singleton class instance
     * Always use this method unless you're working with multiple authenticated users at once
     *
     * @return Codebird The instance
     */
    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self;
        }
        return self::$_instance;
    }
    
    public function getRequestUrl($name) {
        
        return $this->_getEndpoint($name).'/'.$name;
        
    }
    
    public function getRequestCategory($name) {
        
        if (!$name) {
            throw new ErrorException("Method is not defined", 500);
        } 
         
        $categories = array(
            
            self::CATEGORY_AUTH => array(
                'oauth/request_token',
                'oauth/authorize',
            ),
            
            self::CATEGORY_APPLICATION  => array(
                'application/rate_limit_status'
            ),
            self::CATEGORY_FAVORITES    => array(
                'favorites/list',
                'favorites/create',
                'favorites/destroy',
            ),
            self::CATEGORY_FOLLOWERS    => array(
                'followers/ids',
                'followers/list',
            ),
            self::CATEGORY_FRIENDS      => array(
                'friends/ids',
                'friends/list',
            ),
            self::CATEGORY_FRIENDSHIPS  => array(
                'friendships/show',
                'friendships/create',
                'friendships/destroy',
                'friendships/update',
            ),
            self::CATEGORY_HELP         => array(
                'help/configuration',
                'help/languages',
                'help/privacy',
                'help/tos',
            ),
            self::CATEGORY_LISTS        => array(
                'lists/list',
                'lists/members',
                'lists/members/show',
                'lists/memberships',
                'lists/ownerships',
                'lists/show',
                'lists/statuses',
                'lists/subscribers',
                'lists/subscribers/show',
                'lists/subscriptions',
            ),
            self::CATEGORY_SEARCH       => array(
                'search/tweets',
            ),
            self::CATEGORY_STATUSES     => array(
                'statuses/lookup',
                'statuses/oembed',
                'statuses/retweeters/ids',
                'statuses/retweets/:id',
                'statuses/show/:id',
                'statuses/user_timeline',
                'statuses/destroy/:id',
                'statuses/update',
                'statuses/retweet/:id',
                'statuses/update_with_media',
            ),
            self::CATEGORY_TRENDS       => array(
                'trends/available',
                'trends/closest',
                'trends/place',
            ),
            self::CATEGORY_USERS        => array(
                'users/lookup',
                'users/show',
                'users/suggestions',
                'users/suggestions/:slug',
                'users/suggestions/:slug/members',
            ),
            
            self::CATEGORY_MEDIA => array(
                'media/upload',
            )

        );
        
        $category = false;
        foreach ($categories as $key => $methods) {
            if (false !== array_search($name, $methods)) {
                $category = $key;
                break;
            }
        }
        
        if (!$category) {
            throw new ErrorException("Category not found for $name", 500);
        }
        
        return $category;
    }
    
    public function getRequestHttpMethod($method_template, $apiparams) {
        return $this->_detectMethod($method_template, $apiparams);
    }
    
    public function invokeApiMethod($method, $httpmethod, $method_template, $apiparams, $app_only_auth) {
        
        $multipart  = $this->_detectMultipart($method_template);
        $internal   = $this->_detectInternal($method_template);
        
        return $this->_callApi(
            $httpmethod,
            $method,
            $apiparams,
            $multipart,
            $app_only_auth,
            $internal
        );
    } 
    
    public function parseRequest($fn, $params) {
        // parse parameters
        $apiparams = array();
        if (count($params) > 0) {
            if (is_array($params[0])) {
                $apiparams = $params[0];
                if (! is_array($apiparams)) {
                    $apiparams = array();
                }
            } else {
                parse_str($params[0], $apiparams);
                if (! is_array($apiparams)) {
                    $apiparams = array();
                }
                // remove auto-added slashes if on magic quotes steroids
                if (get_magic_quotes_gpc()) {
                    foreach($apiparams as $key => $value) {
                        if (is_array($value)) {
                            $apiparams[$key] = array_map('stripslashes', $value);
                        } else {
                            $apiparams[$key] = stripslashes($value);
                        }
                    }
                }
            }
        }

        // stringify null and boolean parameters
        foreach ($apiparams as $key => $value) {
            if (! is_scalar($value)) {
                continue;
            }
            if (is_null($value)) {
                $apiparams[$key] = 'null';
            } elseif (is_bool($value)) {
                $apiparams[$key] = $value ? 'true' : 'false';
            }
        }

        $app_only_auth = false;
        if (count($params) > 1) {
            $app_only_auth = !! $params[1];
        }

        // reset token when requesting a new token (causes 401 for signature error on 2nd+ requests)
        if ($fn === 'oauth_requestToken') {
            $this->setToken(null, null);
        }

        // map function name to API method
        $method = '';

        // replace _ by /
        $path = explode('_', $fn);
        for ($i = 0; $i < count($path); $i++) {
            if ($i > 0) {
                $method .= '/';
            }
            $method .= $path[$i];
        }
        // undo replacement for URL parameters
        $url_parameters_with_underscore = array('screen_name', 'place_id');
        foreach ($url_parameters_with_underscore as $param) {
            $param = strtoupper($param);
            $replacement_was = str_replace('_', '/', $param);
            $method = str_replace($replacement_was, $param, $method);
        }

        // replace AA by URL parameters
        $method_template = $method;
        $match           = array();
        if (preg_match('/[A-Z_]{2,}/', $method, $match)) {
            foreach ($match as $param) {
                $param_l = strtolower($param);
                $method_template = str_replace($param, ':' . $param_l, $method_template);
                if (! isset($apiparams[$param_l])) {
                    for ($i = 0; $i < 26; $i++) {
                        $method_template = str_replace(chr(65 + $i), '_' . chr(97 + $i), $method_template);
                    }
                    throw new \Exception(
                        'To call the templated method "' . $method_template
                        . '", specify the parameter value for "' . $param_l . '".'
                    );
                }
                $method  = str_replace($param, $apiparams[$param_l], $method);
                unset($apiparams[$param_l]);
            }
        }

        // replace A-Z by _a-z
        for ($i = 0; $i < 26; $i++) {
            $method  = str_replace(chr(65 + $i), '_' . chr(97 + $i), $method);
            $method_template = str_replace(chr(65 + $i), '_' . chr(97 + $i), $method_template);
        }
        
        return array($method, $method_template, $apiparams, $app_only_auth);
    }
    
}
