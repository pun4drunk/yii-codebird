<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ECodebird
 *
 * @author Vladislav
 */

Yii::import('ecodebird.exceptions.*');
Yii::import('ecodebird.CodebirdWrapper');

class ECodebird extends YiiApi\components\Api {
    
    public $name = 'api.twitter';
    public $clientName = 'twitter';
    protected $_initialized=false;
    
    public $returnFormat = 'OBJECT';
    
    public $requestErrors;
    protected $requestAppOnlyAuth = false;
    
    public $useDbRates = false;
    
    const ERROR_BLOCKED = 162;
    
    protected function internalBehaviors() {
        return \CMap::mergeArray(parent::internalBehaviors(), array(
            'apiRates' => array(
                'class' => 'YiiApi\behaviors\ApiRatesBehavior',
            ),
        ));
    }
    
    public function isRawAction($name) {
        return (method_exists($this->_api, $name) || in_array($name, array(
            'oauth_accessToken',
        )));
    }
    
    protected function getApi() {
        
        CodebirdWrapper::setConsumerKey($this->clientConfig['consumer_key'], $this->clientConfig['consumer_secret']);
        $this->_api = CodebirdWrapper::getInstance();
        
        if ($this->returnFormat) {
            $constants = explode(' ', 'OBJECT ARRAY JSON');
            $returnFormat = array_search($this->returnFormat, $constants);
            if (false !== $returnFormat)
                $this->_api->setReturnFormat($returnFormat);
        }
    
        return true;
    }
    
    
    protected function authorizeUser() {
        $this->isAuthorized = false;
        if ($this->clientId = $this->user->getClientId($this->clientName)) {
            
            list($token, $token_secret) = $this->user->getToken($this->clientName);
        
            if (!empty($token) && !empty($token_secret)) {
                $this->_api->setToken($token, $token_secret);
                $this->isAuthorized = true;
            }
        }
        
        return $this->isAuthorized;
    }
    
    public static function getMedia($status, $mediaDir = 'media') {
        
        $result = "";
                
        $media = reset($status->entities->media);
        $url = $media->media_url;

        $ext = substr($url,strrpos($url, '.'));

        $filename = $mediaDir.'/'.self::getImgName($status).$ext;

        if (file_put_contents($filename, file_get_contents($url))) {
            $result = $filename;
        }
        
        return $result;
    }
    
    public function beforeApiRequest() {
        
        $this->requestErrors = array();
        
        list(
            $this->requestMethod, 
            $name, 
            $params,
            $this->requestAppOnlyAuth) = $this->_api->parseRequest(
                    $this->apiRequest->get('name'), $this->apiRequest->get('params'));
        
        $this->apiRequest->setAttributes(array(
            'name'      => $name,
            'params'    => $params,
            'category'  => $this->_api->getRequestCategory($name),
            'method'    => $this->_api->getRequestHttpMethod($name, $params),
            'url'       => $this->_api->getRequestUrl($this->requestMethod),
        ));
        
        return parent::beforeApiRequest();
    }
    
    protected function doApiRequest() {
        
        return $this->_api->invokeApiMethod(
                $this->requestMethod, 
                $this->apiRequest->get('method'), 
                $this->apiRequest->get('name'), 
                $this->apiRequest->get('params'),
                $this->requestAppOnlyAuth
        );
    }
    
    public function getRequestError() {
        
        $errors = $this->apiRequest->get('errors');
        
        if (is_array($errors)) {
            
            $messages = array();
            foreach ($errors as $error) {
                array_push($messages, $error->message);
                if (CodebirdWrapper::ERROR_BLOCKED === $error->code) {
                    throw new CodebirdBlockedException($error->message);
                }
            }
            
            $error = implode(PHP_EOL, $messages);
            
        } else if (is_string($errors)){
            $error = $errors;
        } else {
            $error = "Unknown error";
        }
        return $error;
    }
    
    public function hasMedia($status) {
        return property_exists($status->entities, 'media');
    }
    
    public function isUserProtected($user) {
        return $user->protected;
    }
    
    public function isRetweet($status) {
        return property_exists($status, 'retweeted_status') 
                && is_object($status->retweeted_status);
    }
    
    public static function getMediaUrl($status) {
        $url = NULL;
        if (property_exists($status->entities, 'media') 
                && ($media = reset($status->entities->media))) {
            $url = $media->media_url;
        }
        return $url;
    }
    
    public static function getUserImgUrl($user) {
        return $user->profile_image_url;
    }
    
    protected static function getImgName($status = NULL) {
        //return md5(time();
        return $status->id;
    }
    
}
